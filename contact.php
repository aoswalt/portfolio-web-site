<!DOCTYPE html>
<html>
	<head>
		<title>Adam Oswalt - Contact</title>
		
		<meta charset="utf-8" />
		<meta name="author" content="Adam Oswalt" />
		<meta name="description" content="Adam Oswalt's Portfolio" />
		<meta name="keywords" content="adam, oswalt, portfolio, anger, games" />
		
		<link rel="stylesheet" href="style.css" />
	</head>
	<body>
		<?php include 'header.php' ?>
		
		<section id="content">
			<h1>Contact</h1>
			
			<div class="text-content">
				<p>Email: <a href="mailto:email@adamoswalt.com">email@adamoswalt.com</a></p>
			</div>
		</section>
		
		<?php include 'footer.php' ?>
	</body>
</html>