<!DOCTYPE html>
<html>
	<head>
		<title>Adam Oswalt - Projects</title>
		
		<meta charset="utf-8" />
		<meta name="author" content="Adam Oswalt" />
		<meta name="description" content="Adam Oswalt's Portfolio" />
		<meta name="keywords" content="adam, oswalt, portfolio, anger, games" />
		
		<link rel="stylesheet" href="style.css" />
	</head>
	<body>
		<?php include 'header.php' ?>
		
		<section id="content">
			<h1>Projects</h1>
			
			<ul id="entries">
				<?php include 'project-entries.php' ?>
			</ul>
		</section>
		
		<?php include 'footer.php' ?>
	</body>
</html>