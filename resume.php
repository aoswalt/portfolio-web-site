<!DOCTYPE html>
<html>
	<head>
		<title>Adam Oswalt - Résumé</title>
		
		<meta charset="utf-8" />
		<meta name="author" content="Adam Oswalt" />
		<meta name="description" content="Adam Oswalt's Portfolio" />
		<meta name="keywords" content="adam, oswalt, portfolio, anger, games" />
		
		<link rel="stylesheet" href="style.css" />
	</head>
	<body>
		<?php include 'header.php' ?>
		
		<section id="content">
			<h1>Résumé</h1>
			
			<div class="text-content">
				<div class="center-text"><a class="pdf-link" href="./adam_oswalt-resume.pdf" target="_blank">View PDF</a></div>
				
				<h2>Education</h2>
				<p>ITT Technical Institute, Cordova, TN</p>
				<p>Associate of Applied Science Degree in Information Technology – Multimedia</p>
				<p>Valedictorian GPA: 3.97</p>
				
				<h2>Programming</h2>
				<p>Proficient with C#, SQL, VBA, Java</p>
				<p>Familiar with C/C++, Regular Expressions, HTML/CSS, PHP, JavaScript</p>
				<p>Basic knowledge of Bash, Python</p>
				
				<h2>Projects</h2>
				<p class="outdent"><b>Lettering Automation</b> – Automation of the creation of artwork through templates based on order details</p>
				<p class="outdent"><b>Reporting Tool</b> – A tool to easily get order information and manage custom reports</p>
				<p class="outdent"><b>Particle System</b> – A simple particle system influenced by gravity built using only native Java libraries</p>
				<p class="outdent"><b>Path Builder</b> – A configurable program to get file paths from inconsistent file structure based on order details</p>
				<p class="outdent"><b>Automated Exporter</b> – Using CorelDraw VBA, automated the processing and exporting of inconsistent files</p>
				<p class="outdent"><b>Account Database</b> – Built Access database to fit into office workflow and provide aid through automation</p>
				
				<h2>Technical Skills</h2>
				<p>Proficient with Windows</p>
				<p>Familiar with Linux</p>
				<p>Built multiple personal computers</p>
				<p>Performed basic laptop repair</p>
				<p>Proficient with Microsoft Office Suite</p>
				<p>Proficient with CorelDraw</p>
				<p>Familiar with Photoshop</p>
				<p>Basic knowledge of Illustrator</p>
				<p>Familiar with Blender</p>
				
				<h2>Experience</h2>
				<h3>Programmer / Project Manager of Programming</h3>
				<h4><span class="employ-dates">January 2015 – Present</span>Varsity Spirit, Bartlett, TN</h4>
				<ul>
					<li>Designed and implemented an ongoing automation system for building artwork from order data with C# and CorelDraw VBA</li>
					<li>Created an application for easy retrieval of order information and managing custom reports</li>
					<li>Developed new processes to improve production workflow</li>
				</ul>
				<h3>Senior Artist</h3>
				<h4><span class="employ-dates">July 2012 – December 2014</span>Varsity Spirit, Bartlett, TN</h4>
				<ul>
					<li>Digitized artwork for production, set up templates for new styles, converted styles to fonts</li>
					<li>Created a configurable script in CorelDraw to automate the conversion of files</li>
					<li>Improved general workflow through automation scripting and new processes</li>
				</ul>
				<h3>Customer Service Representative</h3>
				<h4><span class="employ-dates">July 2011 – June 2012</span>Asentinel, Memphis, TN</h4>
				<ul>
					<li>Provided support to customers</li>
					<li>Built a database in Access to automate a significant amount of manual data tracking</li>
				</ul>
				<h3>Graphic Artist</h3>
				<h4><span class="employ-dates">March 2009 – June 2011</span>Varsity Spirit, Bartlett, TN</h4>
				<ul>
					<li>Designed uniforms and lettering based on customer input, interpreted customer information into proper forms</li>
					<li>Wrote a tool for CorelDraw to calculate object area and pricing</li>
				</ul>
			</div>
		</section>
		
		<?php include 'footer.php' ?>
	</body>
</html>