<!DOCTYPE html>
<html>
	<head>
		<title>Adam Oswalt - About Me</title>
		
		<meta charset="utf-8" />
		<meta name="author" content="Adam Oswalt" />
		<meta name="description" content="Adam Oswalt's Portfolio" />
		<meta name="keywords" content="adam, oswalt, portfolio, anger, games" />
		
		<link rel="stylesheet" href="style.css" />
	</head>
	<body>
		<?php include 'header.php' ?>
		
		<section id="content">
			<h1>About Me</h1>
			
			<div class="text-content">
				<h2>The Short</h2>
				<p>I am a guy who makes computer things and solves problems.</p>
			</div>
			
			<div class="text-content">
				<!-- <img src="" /> necessary/good idea? -->
				<h2>The Long</h2>
				<!-- Overview -->
				<p class="indent">
					Hello! My name is Adam Oswalt, and I love all things related to technology and computers.
					One of my greatest strengths is thinking through problems and finding solid solutions to them.
					I am continually full of ideas and strive to give them form. 
				</p>
				<!-- Past -->
				<p class="indent">
					From a very young age, I have been engrossed in computers. 
					I began with using DOS batch files to play games as well as playing around on an NES.
					Later on, I began to experiment with my own development through things like designing maps in Unreal Tournament and working through a book on Visual Basic 6.
					I tinkered around off and on, but the concepts continued to stay rooted and develop.
				</p>
				<!-- Present -->
				<p class="indent">
					Currently, my development projects tend to be focused around automation and quality of life improvements as I also 
					spend time reading and researching to expand my knowledge and gain experience in an effort to strengthen my programming foundation.
				</p>
				<!-- Future -->
				<p class="indent">
					Moving forward, my goal is to pursue my passions of computers and technology.
					I want to surround myself with knowledgable colleagues to help me grow, and 
					I am continuing to work on personal projects to learn and experiment with new ideas.
				</p>
			</div>
		</section>
		
		<?php include 'footer.php' ?>
	</body>
</html>